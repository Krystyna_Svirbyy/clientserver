﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace ServerApp
{
    public sealed class Server
    {
        private static readonly Server INSTANCE = new Server();
        private TcpListener server;
        
        private Server()
        {
            this.server = new TcpListener(IPAddress.Parse("127.0.0.1"), 13000);
        }
        
       public static Server Instance {
           get {
               return INSTANCE;
           }
       }
       
        public void startServer()
        {
            server.Start();
            Console.WriteLine("Server started!");
            
        }

        public void communicateWithClient()
        {
            //Accept tcp clients
            Console.WriteLine("Waiting for Client..."); 
            TcpClient client = this.server.AcceptTcpClient();
            Console.WriteLine("Connected to client"); 
            Console.WriteLine("Waiting for request..."); 
            NetworkStream stream = client.GetStream();
            string request = getRequestFromClient(stream);
            string response = generateResponse(request); 
            sendResponseToClient(response, stream);
            
            client.Close();
            Console.WriteLine();
        }
        
        //Function to get request from clients
        private string getRequestFromClient(NetworkStream stream)
        {
            byte[] bytesRequested = new byte[256];
            int length = stream.Read(bytesRequested, 0, bytesRequested.Length);
            string request = Encoding.ASCII.GetString(bytesRequested, 0, length);
            Console.WriteLine("Got request " + request);
            return request;
        }

        //Function to generate response based on client request
        //"HELLO" replies with "Hi!", "TIME" replies with the current time of the Server 
        //"DIR <directory>" replies with the list of files and directories in the Server for the directory requested
        //If request is different replies with "Bad request"
        private string generateResponse(string request)
        {
            string message = " ";
            switch (request)
            {
                case "HELLO": 
                    message = "Hi!";
                    break;
                case "TIME":
                    DateTime saveUtcNow = DateTime.UtcNow.AddHours(1);
                    message = "Server current time in UTC format is: " + saveUtcNow;
                    break;
                case string a when a.StartsWith("DIR ") && request.Split(" ").Length <= 2 :
                    //string path = Directory.GetCurrentDirectory();
                    string[] directory = request.Split(' ');
                    string path = Environment.CurrentDirectory + directory[1];
                    DirectoryInfo directoryInfo = new DirectoryInfo(path);
                    if (!directoryInfo.Exists)
                    {
                        message = "DIR " + directory[1] + " does not exist";
                        break;
                    }
                    DirectoryInfo[] directoryInfos = directoryInfo.GetDirectories("*");
                    FileInfo[] fileInfo = directoryInfo.GetFiles("*");
                    message = "DIR " + directory[1] + "\n DIRECTORIES: ";
                    foreach (DirectoryInfo d in directoryInfos)
                    {
                        message = message  + d.Name + " ";
                    }
                    message += "\n FILES: ";
                    foreach (FileInfo file in fileInfo)
                    {
                        message = message  + file.Name + " ";
                    }
                    break;
                default:
                    message = message + "Bad request";
                    break;
            }

            return message;
        }

        //Function to send generated response to client
        private void sendResponseToClient(string message, NetworkStream stream)
        {
            
            byte[] bytesSend = Encoding.ASCII.GetBytes(message);
            stream.Write(bytesSend, 0, bytesSend.Length);
            stream.Flush();
                    
            Console.WriteLine("Sent response: " + message);
        }

        public void stopServer()
        {
            server.Stop();
            Console.WriteLine("Server stopped!");
        }


    }
}