﻿using System;
using System.Net.Sockets;

namespace ServerApp
{
    class Program
    {
        private static volatile bool stop_serv = false;
        static void Main(string[] args)
        {
            try
            {
                Server.Instance.startServer();
                Console.CancelKeyPress +=  new ConsoleCancelEventHandler(CancelKeyPressed);
                while (!stop_serv)
                {
                    Server.Instance.communicateWithClient();
                    Console.ReadLine();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        static void CancelKeyPressed(object sender, ConsoleCancelEventArgs consoleCancelEventArgs)
        {
            consoleCancelEventArgs.Cancel = true;
            stop_serv = true;
            Console.WriteLine("Cancel key press...");
            Server.Instance.stopServer();
            Environment.Exit(0);
        }
    }
}