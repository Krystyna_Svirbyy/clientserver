﻿using System;
using System.Net.Sockets;
using System.Text;

namespace ClientApp
{
    public class Client
    {
        private TcpClient client;
        public Client()
        {
            this.client = new TcpClient("127.0.0.1", 13000);
            Console.WriteLine("Client connected to server");
        }

        public void communicateWithServer(string request)
        {
            NetworkStream stream = client.GetStream();
            sendRequestToServer(request, stream);
            Console.WriteLine("Waiting for response from Server...");
            getResponseFromServer(stream);
            
            closeClient();
            Console.WriteLine();
           
        }

        //Function to send request to server
        private void sendRequestToServer(string request, NetworkStream stream)
        {
            Console.WriteLine("Sending request: " + request);
            byte[] bytesWrite = Encoding.ASCII.GetBytes(request);
            stream.Write(bytesWrite, 0, bytesWrite.Length);
            stream.Flush();
            //Console.WriteLine("Client send message: " + bytesWrite.Length);
        }

        //Function to get response from server - return response string 
        private string getResponseFromServer(NetworkStream stream)
        {
            byte[] bytesRead = new byte[256];
            int length = stream.Read(bytesRead, 0, bytesRead.Length);
            string response = Encoding.ASCII.GetString(bytesRead, 0, length);
            Console.WriteLine("Got response from Server:" + response);
            return response;
        }

        public void closeClient()
        {
            this.client.Close();
            Console.WriteLine("Client closed");
        }
    }
}